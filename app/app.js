var BDD = BDD || {};

(function(){

    // App classes:

    function TaskService(taskInteractor){
      this.task = {};
      this.taskInteractor = taskInteractor;

      this.startTask = function(taskTitle){
        this.task.title = taskTitle;
	      this.taskInteractor.renderTask(this.task);
	     };

      this.finishCurrentTask = function() {
          
       };
    };

    function TaskInteractor(widget){
      this.taskTitleWidget = widget;

      this.renderTask = function(task) {
        this.taskTitleWidget.show(task.title);
      };
    };

    // Programmatic interface:

    function App(taskService){
      this.taskService = taskService;

       this.startTask = function(taskTitle){
		      this.taskService.startTask();
       };

       this.finishCurrentTask = function(){
          this.lastFinishedTask = this.taskService.finishCurrentTask();
       };

       this.startUp = function(){
       //    this.messageWidget.show('app started');
       };
    };

    BDD.App = App;
    BDD.TaskService = TaskService;
    BDD.TaskInteractor = TaskInteractor;
}(BDD));
