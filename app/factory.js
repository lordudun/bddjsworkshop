var BDD = BDD || {};

(function(BDD, undefined){

	var createApp = function(){
            var taskService = createTaskService();
            var app = new BDD.App(taskService);
	    return app;
	}

	var createTaskService = function(){
			var taskInteractor = createTaskInteractor();
            return new BDD.TaskService(taskInteractor);
	}

	var createTaskInteractor = function(){
			var widget = new BDD.Widgets.Message('greeting', $('body'));
            return new BDD.TaskInteractor(widget);
	}

	BDD.Factory = BDD.Factory || {};
	BDD.Factory.createApp = createApp;
	BDD.Factory.createTaskService = createTaskService;
	BDD.Factory.createTaskInteractor = createTaskInteractor;

}(BDD));
