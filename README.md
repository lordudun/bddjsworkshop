BDD workshop for RIAs development with JavaScript
-------------------------------------------------

BDD workshop by Carlos Ble (@carlosble)

More information about project in README.txt file

-------------------------------------------------------------

Original code: https://bitbucket.org/carlosble/bddjsworkshop

For more information about the workshop visit http://www.carlosble.com/2013/01/workshop-bdd-for-ria-with-javascript/
