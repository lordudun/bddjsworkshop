describe("the programmatic interface", function(){
	it("asks the task service to start a task", function(){
          var app = BDD.Factory.createApp();

          spyOn(app.taskService, "startTask");

          app.startTask();

          expect(app.taskService.startTask).toHaveBeenCalled();
    });

	it("finish the current task and get the elapsed seconds", function(){
          var app = BDD.Factory.createApp();

          app.taskService.finishCurrentTask = function () {
            return {elapsedSeconds: 2};
         };

          app.finishCurrentTask();

          expect(app.lastFinishedTask.elapsedSeconds).toBe(2);
    });

});

describe("the task service", function(){

	it("asks the interactor to renders a task", function(){
         var taskService = BDD.Factory.createTaskService();

         spyOn(taskService.taskInteractor, "renderTask").andCallFake(function(task){
         	expect(task.title).toEqual("test");
         });

         taskService.startTask("test");

         expect(taskService.taskInteractor.renderTask).toHaveBeenCalled();
    });
    it("asks the interactor to renders a task", function(){
         var taskService = BDD.Factory.createTaskService();

         var called = false;
         taskService.taskInteractor.renderTask = function(task){
         	called = true;
         	expect(task.title).toEqual("test");
         };

         taskService.startTask("test");

         expect(called).toBeTruthy();
    });
});

describe("the task interactor", function(){

	it("asks the binder to show a task title", function(){
         var taskInteractor = BDD.Factory.createTaskInteractor();

         spyOn(taskInteractor.taskTitleWidget, "show");

         taskInteractor.renderTask({title:"test"});

         expect(taskInteractor.taskTitleWidget.show).toHaveBeenCalledWith("test");
    });
});