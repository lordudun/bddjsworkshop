
var ZombieWorld = require('../support/world').World;

var steps = function(){
  var Given = And = When = Then = this.defineStep;
  this.World = ZombieWorld;
 
  this.Before(function(nextStep){
        this.visit('/', nextStep);
  });   

  Given(/^I start working on a report$/, 
    function(nextStep) {
        this.browser.evaluate("BDDApp.startTask('the report')");
        nextStep();
    });

  And(/^I work for (\d+) minutes$/, 
    function(minutes, nextStep) {
        this.browser.wait(2000).then(nextStep);
  });

  When(/^I finish the report$/, 
    function(nextStep) {
        this.browser.evaluate("BDDApp.finishCurrentTask();");
        nextStep();
  });

  Then(/^I want the system to tell me that I have spent (\d+) minutes working on the report$/, 
    function(minutes, nextStep) {
        this.browser.evaluate('BDDApp.lastFinishedTask.elapsedSeconds').should.be(2);
        nextStep();
  });
}

module.exports = steps;

