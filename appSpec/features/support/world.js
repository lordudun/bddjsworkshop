var zombie = require('zombie'),
    should = require('should');

var ZombieWorld = function(callback){
  this.browser = new zombie.Browser({runScripts:true, debug:true});

  this.page = function(path){
   return "http://localhost:3000";
  };

  this.visit = function(path, callback){
    this.browser.visit( this.page(path), function(err, browser, status){
      callback(err, browser, status);
    });
  };
  callback();
};

exports.World = ZombieWorld;
