Feature: Tasks management
  As a freelance, 
  I want to keep track of my work 
  so that I can charge my customers precisely and optimize my processes.

  Scenario: counting the time spent on a task
    Given I start working on a report
    And I work for 30 minutes
    When I finish the report
    Then I want the system to tell me that I have spent 30 minutes working on the report


