var app, appAddress, initTests, loadApp, should, two;
app = null;
should = chai.should();
loadApp = sOn.Testing.Integration.loadTargetWindow;
appAddress = 'http://localhost:3000/';
initTests = function(appWin) {
  return app = appWin.BDDApp;
};
two = {
  seconds: 2000
};

describe("LiveTeamApp: the team productivity application", function() {

  beforeEach(function() {
    loadApp(appAddress, initTests);
  });

  describe("the tasks management system", function() {
    it("counts the time consumed by every task", function() {
      app.startTask("attending the webinar");
      waits(two.seconds);
      runs(function() {
        app.finishCurrentTask();
        app.lastFinishedTask.elapsedSeconds.should.be(2);
      });
    });
  });
});